msgid ""
msgstr ""
"Project-Id-Version: ajenti\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-09-18 15:51+0200\n"
"PO-Revision-Date: 2016-05-21 17:19-0400\n"
"Last-Translator: eugenepankov <john.pankov@gmail.com>\n"
"Language-Team: Spanish\n"
"Language: es_ES\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: ajenti\n"
"X-Crowdin-Language: es-ES\n"
"X-Crowdin-File: /2.0/plugins.po\n"

#: plugins/plugins/resources/partial/index.html:14
msgid "Ajenti core {{ajentiVersion}}, no upgrades available."
msgstr ""

#: plugins/plugins/resources/partial/index.html:20
msgid "Ajenti core {{ajentiVersion}}. An upgrade to {{coreUpgradeAvailable}} is available."
msgstr ""

#: plugins/plugins/resources/partial/index.html:141
#: plugins/plugins/resources/partial/index.html:196
#: plugins/plugins/resources/partial/repo-plugin.html:47
msgid "Author"
msgstr ""

#: plugins/plugins/resources/partial/index.html:53
msgid "Available plugins"
msgstr ""

#: plugins/plugins/resources/partial/index.html:160
#: plugins/plugins/resources/partial/index.html:212
msgid "Close"
msgstr ""

#: plugins/plugins/resources/partial/index.html:76
msgid "Community"
msgstr ""

#: plugins/plugins/resources/partial/index.html:10
msgid "Core"
msgstr ""

#: plugins/plugins/resources/partial/index.html:190
msgid "Downloads in the last month"
msgstr ""

#: plugins/plugins/resources/partial/repo-plugin.html:39
msgid "Downloads this month"
msgstr ""

#: plugins/plugins/resources/partial/index.html:109
msgid "Error"
msgstr ""

#: plugins/plugins/resources/partial/index.html:120
msgid "Exception: <code>{{selectedInstalledPlugin.crash.cls}}</code>"
msgstr ""

#: plugins/plugins/resources/partial/index.html:77
msgid "Filter"
msgstr ""

#: plugins/plugins/resources/partial/index.html:148
#: plugins/plugins/resources/partial/repo-plugin.html:55
msgid "Homepage"
msgstr ""

#: plugins/plugins/resources/partial/index.html:209
#: plugins/plugins/resources/partial/index.html:62
#: plugins/plugins/resources/partial/index.html:80
msgid "Install"
msgstr ""

#: plugins/plugins/resources/partial/index.html:27
msgid "Installed plugins"
msgstr ""

#: plugins/plugins/resources/partial/index.html:47
msgid "Loading error"
msgstr ""

#: plugins/plugins/resources/partial/index.html:135
msgid "Location"
msgstr ""

#: plugins/plugins/resources/partial/index.html:123
msgid "Message: <code>{{selectedInstalledPlugin.crash.message}}</code>"
msgstr ""

#: plugins/plugins/resources/partial/index.html:103
#: plugins/plugins/resources/partial/index.html:172
#: plugins/plugins/resources/partial/repo-plugin.html:7
msgid "Name"
msgstr ""

#: plugins/plugins/resources/partial/index.html:59
msgid "Official"
msgstr ""

#: plugins/plugins/resources/partial/index.html:221
msgid "Restart panel"
msgstr ""

#: plugins/plugins/resources/partial/index.html:118
msgid "This plugin crashed with the following error:"
msgstr ""

#: plugins/plugins/resources/partial/index.html:113
msgid "This plugin requires application binary <code>{{selectedInstalledPlugin.crash.binaryName}}</code>, which was unavailable during startup."
msgstr ""

#: plugins/plugins/resources/partial/index.html:110
msgid "This plugin requires plugin <code>{{selectedInstalledPlugin.crash.pluginName}}</code>, which was unavailable during startup."
msgstr ""

#: plugins/plugins/resources/partial/repo-plugin.html:15
msgid "Title"
msgstr ""

#: plugins/plugins/resources/partial/index.html:157
#: plugins/plugins/resources/partial/index.html:33
#: plugins/plugins/resources/js/controllers/index.controller.coffee:143
msgid "Uninstall"
msgstr ""

#: plugins/plugins/resources/partial/index.html:154
#: plugins/plugins/resources/partial/index.html:23
#: plugins/plugins/resources/partial/index.html:36
msgid "Update"
msgstr ""

#: plugins/plugins/resources/partial/index.html:6
msgid "Update everything"
msgstr ""

#: plugins/plugins/resources/partial/index.html:3
msgid "Updates"
msgstr ""

#: plugins/plugins/resources/partial/index.html:44
msgid "Upgradeable to {{getUpgrade(plugin).version}}"
msgstr ""

#: plugins/plugins/resources/partial/index.html:129
#: plugins/plugins/resources/partial/index.html:178
#: plugins/plugins/resources/partial/repo-plugin.html:23
msgid "Version"
msgstr ""

#: plugins/plugins/main.py:15
msgid "Plugins"
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:25
msgid "Could not load plugin repository"
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:27
msgid "Could not load the installed plugin list"
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:38
msgid "Upgrading"
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:40
#: plugins/plugins/resources/js/controllers/index.controller.coffee:74
#: plugins/plugins/resources/js/controllers/index.controller.coffee:125
#: plugins/plugins/resources/js/controllers/index.controller.coffee:147
msgid "Done"
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:40
msgid "Upgrade complete. A panel restart is absolutely required."
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:40
#: plugins/plugins/resources/js/controllers/index.controller.coffee:74
#: plugins/plugins/resources/js/controllers/index.controller.coffee:125
#: plugins/plugins/resources/js/controllers/index.controller.coffee:147
msgid "Restart now"
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:43
msgid "Upgrade failed"
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:73
msgid "All plugins updated"
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:74
#: plugins/plugins/resources/js/controllers/index.controller.coffee:125
msgid "Installed. A panel restart is required."
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:74
#: plugins/plugins/resources/js/controllers/index.controller.coffee:125
#: plugins/plugins/resources/js/controllers/index.controller.coffee:147
msgid "Later"
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:77
msgid "Some plugins failed to update"
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:97
msgid "Updating plugins"
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:129
msgid "Install failed"
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:135
msgid "Warning"
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:135
msgid "This will remove the Plugins plugin. You can reinstall it later using PIP."
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:135
msgid "Continue"
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:135
#: plugins/plugins/resources/js/controllers/index.controller.coffee:143
msgid "Cancel"
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:143
msgid "Uninstall #{plugin.name}?"
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:144
msgid "Uninstalling"
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:147
msgid "Uninstalled. A panel restart is required."
msgstr ""

#: plugins/plugins/resources/js/controllers/index.controller.coffee:150
msgid "Uninstall failed"
msgstr ""

