msgid ""
msgstr ""
"Project-Id-Version: ajenti\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-08-04 13:39+0200\n"
"PO-Revision-Date: 2016-08-04 07:37-0400\n"
"Last-Translator: eugenepankov <john.pankov@gmail.com>\n"
"Language-Team: German\n"
"Language: de_DE\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: ajenti\n"
"X-Crowdin-Language: de\n"
"X-Crowdin-File: /2.0/core.po\n"

#: plugins/core/content/pages/index.html:144
msgid "A bootstrap error has occured"
msgstr "Ein Initialisierungsfehler ist aufgetreten"

#: plugins/core/resources/partial/login.html:27
msgid "Authenticate"
msgstr "Authentifizieren"

#: plugins/core/content/pages/index.html:82
msgid "Elevate"
msgstr "Erhöhen"

#: plugins/core/content/pages/index.html:127
msgid "Enter"
msgstr "Eingeben"

#: plugins/core/resources/partial/login.html:26
msgid "Log in"
msgstr "Anmelden"

#: plugins/core/resources/partial/login.html:31
msgid "Log in with e-mail"
msgstr "Mit Ihrer E-Mailadresse anmelden"

#: plugins/core/content/pages/index.html:87
msgid "Log out"
msgstr "Abmelden"

#: plugins/core/resources/partial/login.html:22
msgid "Password"
msgstr "Passwort"

#: plugins/core/content/pages/index.html:147
msgid "Please see browser console"
msgstr "Bitte schauen Sie in die Browser Konsole"

#: plugins/core/content/pages/index.html:123
msgid "Quick search"
msgstr "Schnellsuche"

#: plugins/core/resources/partial/login.html:19
msgid "Username"
msgstr "Benutzername"

#: plugins/core/main.py:17
msgid "General"
msgstr "Allgemein"

#: plugins/core/main.py:24
msgid "Tools"
msgstr "Werkzeuge"

#: plugins/core/main.py:31
msgid "Software"
msgstr "Software"

#: plugins/core/main.py:38
msgid "System"
msgstr "System"

#: plugins/core/main.py:45
msgid "Other"
msgstr "Sonstiges"

#: plugins/core/main.py:68
msgid "Read configuration file"
msgstr "Konfiguration-Datei lesen"

#: plugins/core/main.py:73
msgid "Write configuration file"
msgstr "Konfigurationsdatei schreiben"

#: plugins/core/views/api.py:97
msgid "Authorization failed"
msgstr "Autorisierung fehlgeschlagen"

#: plugins/core/views/api.py:121
#, python-format
msgid "Could not authenticate with Mozilla Persona: %s"
msgstr "Konnte nicht mit Mozilla Persona: %s authentifiziert werden"

#: plugins/core/views/api.py:135
msgid "Unrecognized e-mail"
msgstr "Unbekannte E-Mailadresse"

#: plugins/core/resources/js/core/filters.coffee:6
msgid "0 bytes"
msgstr "0 Bytes"

#: plugins/core/resources/js/core/filters.coffee:10
msgid "bytes"
msgstr "bytes"

#: plugins/core/resources/js/core/filters.coffee:11
msgid "KB"
msgstr "KB"

#: plugins/core/resources/js/core/filters.coffee:12
msgid "MB"
msgstr "MB"

#: plugins/core/resources/js/core/filters.coffee:13
msgid "GB"
msgstr "GB"

#: plugins/core/resources/js/core/filters.coffee:14
msgid "TB"
msgstr "TB"

#: plugins/core/resources/js/core/filters.coffee:15
msgid "PB"
msgstr "PB"

#: plugins/core/resources/js/core/filters.coffee:34
msgid "th"
msgstr "."

#: plugins/core/resources/js/core/filters.coffee:35
msgid "st"
msgstr "."

#: plugins/core/resources/js/core/filters.coffee:36
msgid "nd"
msgstr "."

#: plugins/core/resources/js/core/filters.coffee:37
msgid "rd"
msgstr "."

#: plugins/core/resources/js/core/interceptors.coffee:9
msgid "Security error"
msgstr "Sicherheitsfehler"

#: plugins/core/resources/js/core/interceptors.coffee:12
msgid "Server error"
msgstr "Server-Fehler"

#: plugins/core/resources/js/core/interceptors.coffee:12
msgid "Close"
msgstr "Schließen"

#: plugins/core/resources/js/core/interceptors.coffee:19
msgid "Your session has expired"
msgstr "Ihre Sitzung ist abgelaufen"

#: plugins/core/resources/js/core/controllers/login.controller.coffee:22
#: plugins/core/resources/js/core/controllers/login.controller.coffee:39
msgid "Authentication failed"
msgstr "Authentifizierung fehlgeschlagen"

#: plugins/core/resources/js/core/controllers/login.controller.coffee:35
msgid "Authentication succeeded"
msgstr "Authentifizierung erfolgreich"

#: plugins/core/resources/js/core/services/core.service.coffee:7
msgid "Restart"
msgstr "Neustarten"

#: plugins/core/resources/js/core/services/core.service.coffee:8
msgid "Restart the panel?"
msgstr "Das Panel neustarten?"

#: plugins/core/resources/js/core/services/core.service.coffee:9
msgid "Yes"
msgstr "Ja"

#: plugins/core/resources/js/core/services/core.service.coffee:10
msgid "No"
msgstr "Nein"

#: plugins/core/resources/js/core/services/core.service.coffee:16
msgid "Restarting"
msgstr "Neustart wird durchgeführt"

#: plugins/core/resources/js/core/services/core.service.coffee:21
msgid "Restarted"
msgstr "Neu gestartet"

#: plugins/core/resources/js/core/services/core.service.coffee:21
msgid "Please wait"
msgstr "Bitte warten"

#: plugins/core/resources/js/core/services/core.service.coffee:30
msgid "Could not restart"
msgstr "Konnte nicht neu gestartet werden"

#: plugins/core/resources/js/core/services/tasks.service.coffee:25
msgid "Done"
msgstr "Fertig"

#: plugins/core/resources/js/core/services/tasks.service.coffee:30
msgid "Failed: #{msg.message.exception}"
msgstr "Fehler: #{msg.message.exception}"
