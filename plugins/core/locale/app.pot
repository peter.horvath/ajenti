# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-08-04 13:39+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: plugins/core/content/pages/index.html:144
msgid "A bootstrap error has occured"
msgstr ""

#: plugins/core/resources/partial/login.html:27
msgid "Authenticate"
msgstr ""

#: plugins/core/content/pages/index.html:82
msgid "Elevate"
msgstr ""

#: plugins/core/content/pages/index.html:127
msgid "Enter"
msgstr ""

#: plugins/core/resources/partial/login.html:26
msgid "Log in"
msgstr ""

#: plugins/core/resources/partial/login.html:31
msgid "Log in with e-mail"
msgstr ""

#: plugins/core/content/pages/index.html:87
msgid "Log out"
msgstr ""

#: plugins/core/resources/partial/login.html:22
msgid "Password"
msgstr ""

#: plugins/core/content/pages/index.html:147
msgid "Please see browser console"
msgstr ""

#: plugins/core/content/pages/index.html:123
msgid "Quick search"
msgstr ""

#: plugins/core/resources/partial/login.html:19
msgid "Username"
msgstr ""

#: plugins/core/main.py:17
msgid "General"
msgstr ""

#: plugins/core/main.py:24
msgid "Tools"
msgstr ""

#: plugins/core/main.py:31
msgid "Software"
msgstr ""

#: plugins/core/main.py:38
msgid "System"
msgstr ""

#: plugins/core/main.py:45
msgid "Other"
msgstr ""

#: plugins/core/main.py:68
msgid "Read configuration file"
msgstr ""

#: plugins/core/main.py:73
msgid "Write configuration file"
msgstr ""

#: plugins/core/views/api.py:97
msgid "Authorization failed"
msgstr ""

#: plugins/core/views/api.py:121
#, python-format
msgid "Could not authenticate with Mozilla Persona: %s"
msgstr ""

#: plugins/core/views/api.py:135
msgid "Unrecognized e-mail"
msgstr ""

#: plugins/core/resources/js/core/filters.coffee:6
msgid "0 bytes"
msgstr ""

#: plugins/core/resources/js/core/filters.coffee:10
msgid "bytes"
msgstr ""

#: plugins/core/resources/js/core/filters.coffee:11
msgid "KB"
msgstr ""

#: plugins/core/resources/js/core/filters.coffee:12
msgid "MB"
msgstr ""

#: plugins/core/resources/js/core/filters.coffee:13
msgid "GB"
msgstr ""

#: plugins/core/resources/js/core/filters.coffee:14
msgid "TB"
msgstr ""

#: plugins/core/resources/js/core/filters.coffee:15
msgid "PB"
msgstr ""

#: plugins/core/resources/js/core/filters.coffee:34
msgid "th"
msgstr ""

#: plugins/core/resources/js/core/filters.coffee:35
msgid "st"
msgstr ""

#: plugins/core/resources/js/core/filters.coffee:36
msgid "nd"
msgstr ""

#: plugins/core/resources/js/core/filters.coffee:37
msgid "rd"
msgstr ""

#: plugins/core/resources/js/core/interceptors.coffee:9
msgid "Security error"
msgstr ""

#: plugins/core/resources/js/core/interceptors.coffee:12
msgid "Server error"
msgstr ""

#: plugins/core/resources/js/core/interceptors.coffee:12
msgid "Close"
msgstr ""

#: plugins/core/resources/js/core/interceptors.coffee:19
msgid "Your session has expired"
msgstr ""

#: plugins/core/resources/js/core/controllers/login.controller.coffee:22
#: plugins/core/resources/js/core/controllers/login.controller.coffee:39
msgid "Authentication failed"
msgstr ""

#: plugins/core/resources/js/core/controllers/login.controller.coffee:35
msgid "Authentication succeeded"
msgstr ""

#: plugins/core/resources/js/core/services/core.service.coffee:7
msgid "Restart"
msgstr ""

#: plugins/core/resources/js/core/services/core.service.coffee:8
msgid "Restart the panel?"
msgstr ""

#: plugins/core/resources/js/core/services/core.service.coffee:9
msgid "Yes"
msgstr ""

#: plugins/core/resources/js/core/services/core.service.coffee:10
msgid "No"
msgstr ""

#: plugins/core/resources/js/core/services/core.service.coffee:16
msgid "Restarting"
msgstr ""

#: plugins/core/resources/js/core/services/core.service.coffee:21
msgid "Restarted"
msgstr ""

#: plugins/core/resources/js/core/services/core.service.coffee:21
msgid "Please wait"
msgstr ""

#: plugins/core/resources/js/core/services/core.service.coffee:30
msgid "Could not restart"
msgstr ""

#: plugins/core/resources/js/core/services/tasks.service.coffee:25
msgid "Done"
msgstr ""

#: plugins/core/resources/js/core/services/tasks.service.coffee:30
msgid "Failed: #{msg.message.exception}"
msgstr ""
